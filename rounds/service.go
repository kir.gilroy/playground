package rounds

import (
	"fmt"
)

type PlayerRepository interface {
	PlayerName() string
}

type EventRepository interface {
	AllEvents() []string
}

type Repositories struct {
	Player PlayerRepository
	Events EventRepository
}

type Service struct {
	repos Repositories
}

func New(repos Repositories) Service {
	return Service{repos}
}

func (s Service) Rounds() {
	for _, event := range s.repos.Events.AllEvents() {
		fmt.Printf("Player %s has event %s\n", s.repos.Player.PlayerName(), event)
	}
}
