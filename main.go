package main

import (
	"gitlab.com/everumnv/playground/account"
	"gitlab.com/everumnv/playground/rounds"
	"gitlab.com/everumnv/playground/storage/accounts"
	"gitlab.com/everumnv/playground/storage/cacher"
	"gitlab.com/everumnv/playground/storage/player"
)

func main() {
	accountRepo := accounts.Repository{
		Kind: "green",
	}
	playerRepo := player.Repository{
		Name: "janedoe",
	}

	repos := account.Repositories{
		Player:  playerRepo,
		Account: accountRepo,
	}

	roundsFactory := func(events []string) account.Rounder {
		cache := cacher.New(events)
		roundRepos := rounds.Repositories{
			Player: playerRepo,
			Events: cache.EventsRepo(),
		}
		return rounds.New(roundRepos)
	}

	svc := account.New(repos, roundsFactory)
	svc.Run()
}
