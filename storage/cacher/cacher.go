package cacher

type EventsRepo struct {
	events []string
}

func (er EventsRepo) AllEvents() []string {
	return er.events
}

type Instance struct {
	events []string
}

func New(events []string) Instance {
	return Instance{events}
}

func (i Instance) EventsRepo() EventsRepo {
	return EventsRepo{i.events}
}
