package accounts

type Repository struct {
	Kind string
}

func (r Repository) AccountKind() string {
	return "Account kind: " + r.Kind
}
