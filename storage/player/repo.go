package player

type Repository struct {
	Name string
}

func (r Repository) PlayerName() string {
	return "Player name: " + r.Name
}
