package storage

type PlayersRepository interface {
	PlayerName() string
}

type AccountRepository interface {
	AccountKind() string
}

type Repositories struct {
	Players PlayersRepository
	Account AccountRepository
}

func (r Repositories) GetPlayers() PlayersRepository {
	return r.Players
}

func (r Repositories) GetAccount() AccountRepository {
	return r.Account
}
