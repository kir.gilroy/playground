# Proof of concept for factory function approach

## Entities

### `account` service

`account` service needs:

* `PlayerRepository`
* `AccountRepository`

and it also needs to construct `rounds` service, initialising it with an array of `events`.

### `bonus` service

`bonus` service needs:

* `PlayerRepository`
* `EventRepository`

### caching

`storage/cacher` simulates a caching layer. It accepts an array of `events` and returns
`EventRepository` that allows its clients to list the events.

## main.go

`main.go` acts as an application constructor, including:

* it creates `storage/accounts` and `storage/players` repositories
* it provides `roundsFactory` factory function, which in turn:
    * accepts an array of `events`
    * creates a `cacher`
    * instantiates `rounds` services using `storage/player` repository from a closure, and
      `storage/events` repository returned by the cacher

## Dependency tree

Well, there isn't one actually. The end result is that services and repositories have no direct
dependencies between each other – `main` constructs everything and passes them down as parameters.

I decided to stick with `Repositories` structures for all services to illustrate this concept – I
think it's more concise and readable (especially considering that in real system we have a dozen of
those) than listing all repos as individual parameters.
