package account

import (
	"fmt"
)

type PlayerRepository interface {
	PlayerName() string
}

type AccountRepository interface {
	AccountKind() string
}

type Repositories struct {
	Player  PlayerRepository
	Account AccountRepository
}

type Rounder interface {
	Rounds()
}

type RoundsFactory func([]string) Rounder

type Service struct {
	repos         Repositories
	roundsFactory RoundsFactory
}

func New(repos Repositories, rf RoundsFactory) Service {
	return Service{repos, rf}
}

func (s Service) Run() {
	events := []string{"bet", "win"}
	rounds := s.roundsFactory(events)

	fmt.Printf("Player: %s\n", s.repos.Player.PlayerName())
	rounds.Rounds()
}
